<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1,maximum-scale=1,minimum-scale=1 user-scalable=no,width = 320" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="default" />

<body onload="setTimeout(function() { window.scrollTo(0, 1) }, 100);"></body>

<title>Please select your stop:</title>
<style type="text/css">
<!--
body {
	background-color: #333;
	margin: 0px;
	color: #FFF;
}
a {
	font-size: smaller;
	color: #FFF;
	background-color:#82291E;
	padding:5px;
	text-decoration: none;
	margin: 5px;
	line-height: 2em;
}
a:link {
	font-size: smaller;
	color: #FFF;
	background-color:#82291E;
	padding: 5px;
	text-decoration: none;
}
.container {
	background-image:url(Images/timefinderfooter.jpg);
	width: 320px;
	text-align: center;
}
.item {
	margin-right: 5px;
}
h1 {
	padding: 0px;
	margin: 0px;
	line-height: 75px;
}
h4 {
	padding: 0px;
	margin: 0px;
	line-height: 110px;
}
	
.header {
	margin: 0px;
	padding: 0px;
	width: 320px;
	height: 76px;
	background-image:url(Images/timefinderhead.jpg);
}
.footer {
	width: 320px;
	height: 40px;
	background-image:url(Images/timefinderfooter.jpg);
}
#search {
	margin-top: 0px;
	line-height: 100px;
}
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
}
#apDiv1 {
	position:absolute;
	left:10px;
	top:19px;
	width:70px;
	height:50px;
	z-index:1;
}
-->
</style></head>

<body>

<div id="apDiv1"><form class="form" action="http://timefinder.org" method="post">
      <input type="image" src="Images/back.png" border="0" name="submit" /></form></div>
<div class="container">
<?php if (isset($_REQUEST["service"])) {
	$n = $_REQUEST["service"];
	$journey = $_REQUEST["journey"];
	?>
<div class="header"><h1><?=$n?></h1></div>
<?php	
	if (file_exists("routes/".$n."/".$journey.".csv")) {
?>
<p>Please select your stop:</p>
<?php		$doneids = array();
		$stops = fopen("routes/".$n."/".$journey.".csv", "r");
		$journey = str_replace(".csv", "", $journey); ?>
		<?php	while ($stop = fgetcsv($stops)) {
			if (!in_array($stop[0], $doneids) and file_exists("stops/".$stop[0].".csv")) {
				$doneids[] = $stop;
	?>
        <a href='stop.php?stop=<?=$stop[0]?>&service=<?=$n?>&name=<?=$stop[1]?>'><?=$stop[1]?></a><p>
<?php } } } else { ?>
	<div class="header"><h4>Sorry, We didn't find you</h4></div>
<?php } } else { ?>
	<div class="header"><h4>You didn't submit a bus number.</h4></div>
<?php } ?>
<div class="footer"></div>
</div>
</body>
</html>
