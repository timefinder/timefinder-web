<?php 
header ("Content-Type: text/csv");
if (isset($_REQUEST["service"])) {
	$n = $_REQUEST["service"];
	$journey = $_REQUEST["journey"];
	if (file_exists("routes/".$n."/".$journey.".csv")) {
		$doneids = array();
		$stops = fopen("routes/".$n."/".$journey.".csv", "r");
		$journey = str_replace(".csv", "", $journey);
		while ($stop = fgetcsv($stops)) {
			if (!in_array($stop[0], $doneids) and file_exists("stops/".$stop[0].".csv")) {
				$doneids[] = $stop;
				if (file_exists("latlng/".$stop[0].".csv")) {
					$f = fopen("latlng/".$stop[0].".csv", "r");
					list($lat,$lon) = fgetcsv($f);
					fclose($f);
				}
				else {
				}
				if ($lat && $lon) {
	echo $stop[0].",".$stop[1].",".$lat.",".$lon."\n";
} } } } }

?>
