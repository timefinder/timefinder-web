<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1,maximum-scale=1,minimum-scale=1 user-scalable=no,width = 320" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="default" />

<title>Please select your stop:</title>
<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0px; padding: 0px }
  #map_canvas { height: 100% }
</style>

<script type="text/javascript"
    src="http://maps.google.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript">
  function initialize() {
    var latlng = new google.maps.LatLng(53.5, -2);
    var myOptions = {
      zoom: 8,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"),
        myOptions);
    
    markersArray = [];
    var bounds = new google.maps.LatLngBounds(); 
    //busicon = "http://maps.google.com/mapfiles/ms/micons/bus.png"
    
<?php if (isset($_REQUEST["service"])) {
	$n = $_REQUEST["service"];
	$journey = $_REQUEST["journey"];
	if (file_exists("routes/".$n."/".$journey.".csv")) {
		$doneids = array();
		$stops = fopen("routes/".$n."/".$journey.".csv", "r");
		$journey = str_replace(".csv", "", $journey);
		while ($stop = fgetcsv($stops)) {
			if (!in_array($stop[0], $doneids) and file_exists("stops/".$stop[0].".csv")) {
				$doneids[] = $stop;
				if (file_exists("latlng/".$stop[0].".csv")) {
					$f = fopen("latlng/".$stop[0].".csv", "r");
					list($lat,$lon) = fgetcsv($f);
					fclose($f);
				}
				else {
				}
				if ($lat && $lon) {
	?>
    latlng = new google.maps.LatLng(<?=$lat?>, <?=$lon?>);
    bounds.extend(latlng); 
    var marker = new google.maps.Marker({
	map: map, 
	position: latlng,
	title: "<?=trim($stop[1])?>"
	//icon: busicon
    });
    google.maps.event.addListener(marker, 'click', function() {
	document.location = "stop.php?stop=<?=$stop[0]?>&service=<?=$n?>&name=<?=trim($stop[1])?>";
    })
<?php } } } } } ?>
     
     map.fitBounds(bounds); 

  }
</script>

</head>

<body onload="initialize()">

<div id="map_canvas" style="width:100%; height:100%"></div>

</body>
</html>