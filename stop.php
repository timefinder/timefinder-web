<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1,maximum-scale=1,minimum-scale=1 user-scalable=no,width = 320" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="default" />

<body onload="setTimeout(function() { window.scrollTo(0, 1) }, 100);"></body>

<title>Your Timetable</title>
<style type="text/css">
<!--
body {
	background-color: #333;
	margin: 0px;
	color: #FFF;
}
a {
	font-size: smaller;
	color: #FFF;
	background-color:#82291E;
	padding:5px;
	text-decoration: none;
	margin: 5px;
	line-height: 2em;
}
a:link {
	font-size: smaller;
	color: #FFF;
	background-color:#82291E;
	padding: 5px;
	text-decoration: none;
}
.container {
	background-image:url(Images/timefinderfooter.jpg);
	width: 320px;
	text-align: center;
}
.item {
	margin-right: 5px;
}
h1 {
	padding: 0px;
	margin: 0px;
	line-height: 75px;
}
	
.header {
	margin: 0px;
	padding: 0px;
	width: 320px;
	height: 76px;
	background-image:url(Images/timefinderhead.jpg);
}
.footer {
	width: 320px;
	height: 40px;
	background-image:url(Images/timefinderfooter.jpg);
}
#search {
	margin-top: 0px;
	line-height: 100px;
}
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
}
a {
	font-size: smaller;
	color: #333;
	background-color:#6C9;
	padding: 10px;
	margin: 5px;
	text-decoration: none;
}
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: none;
}
a:active {
	text-decoration: none;
}
#apDiv1 {
	position:absolute;
	left:10px;
	top:19px;
	width:70px;
	height:50px;
	z-index:1;
}
-->
</style></head>

<body>

<div id="apDiv1"><form class="form" action="http://timefinder.org" method="post">
      <input type="image" src="Images/back.png" border="0" name="submit" /></form></div>
<div class="container">
<div class="header"><h1><? if(isset($_REQUEST["service"])) echo $_REQUEST["service"];?></h1></div>
  <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
    <?php
	#$dayarr = array("", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
	$dayarr = array("", "", "Mon", "Tues", "Wed", "Thur", "Fri", "Sat", "Sun");
	function m($num) { if ($num>2) return $num-1; else return 8; }
	function p($num) { if ($num<8) return $num+1; else return 2; }
	
        if (isset($_REQUEST["stop"]) and isset($_REQUEST["service"])and file_exists("stops/".$_REQUEST["stop"].".csv")) {
            $stop = $_REQUEST["stop"];
            $service = $_REQUEST["service"];
	    $stops = fopen("stops/".$stop.".csv", "r");
	    if (isset($_REQUEST["rday"])) $rday = $_REQUEST["rday"];
            else $rday = date("N")+1;
    ?>
    <tr>
      <th height="18" colspan="3" align="center" valign="middle" scope="col">&nbsp;</th>
    </tr>
    <tr>
      <td align="left" valign="middle" width="33%"><a href="?stop=<?=$stop?>&service=<?=$service?>&name=<?=$_REQUEST["name"]?>&rday=<?=m($rday)?>">&lt;- <?=$dayarr[m($rday)]?></a></td>
      <td align="center" valign="middle" width="33%"><?=$dayarr[$rday]?></td>
      <td align="right" valign="middle" width="33%"><a href="?stop=<?=$stop?>&service=<?=$service?>&name=<?=$_REQUEST["name"]?>&rday=<?=p($rday)?>"><?=$dayarr[p($rday)]?> -&gt;</a></td>
    </tr>
    <tr>
      <td colspan="3" align="center" valign="middle"><h5><?=$_REQUEST["name"]?></h5></td>
    </tr>
    <tr>
    <?php
            $i = 0;
	    $latsh = "";
            while ($row = fgetcsv($stops)) {
                if ($row[0] == $service and $row[$rday] == "1") {
			$h = substr($row[1], 0, 2);
			if ($h == $lasth) $time = $row[1];
			else $time = "<b>".$h."</b>".substr($row[1], 2, 2);
			$lasth = $h;
			?>
      <td align="center" valign="middle"><?=$time?></td>
                <?php $i+=1;
                    if ($i >= 3) {
                        $i = 0 ?>
    </tr>
    <tr>
                <?php }
                }
            }
    ?>
    </tr>
    <?php } else { ?>
    <tr>
      <th colspan="3" align="center" valign="middle" scope="col"><h1>Not Found</h1></th>
    </tr>
    <?php } ?>
  </table> 

<div class="footer"></div> 


</div>
</body>
</html>